[Verse 1]
Look at the dead outside my window
Wonder what's on their mind
Why do they run?
They all seem to have a mission
But then they cry themselves to sleep